#include "Lista.h"
using namespace std;

Lista::Lista(): _head(nullptr), _last(nullptr), _lenght(0){}

Lista::Lista(const Lista& l) : Lista() {
    *this = l;
}

Lista::~Lista() {
    while (longitud() != 0) {
        eliminar(0);
    }
    if (_head != nullptr) {
        delete _head;
        _head = nullptr;
        delete _last;
        _last = nullptr;
    }
}

Lista& Lista::operator=(const Lista& aCopiar) {
    while (this->longitud() != 0) {
        eliminar(0);
    }
    for (int i = 0; i < aCopiar.longitud(); i++) {
        this->agregarAtras(aCopiar.iesimo(i));
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    Nodo *nuevoNodo = new Nodo;
    (_head != nullptr) ? _head->_prev = nuevoNodo : nullptr;
    nuevoNodo->_next = _head;
    nuevoNodo->_prev = nullptr;
    nuevoNodo->_data = elem;
    _head = nuevoNodo;
    _lenght++;
    if (longitud() == 1){
        _last = nuevoNodo;
    }
}

void Lista::agregarAtras(const int& elem) {
    Nodo* nuevoNodo = new Nodo;
    nuevoNodo->_prev = _last;
    nuevoNodo->_next = nullptr;
    nuevoNodo->_data = elem;
    (_last != nullptr) ? _last->_next = nuevoNodo : nullptr;
    _last = nuevoNodo;
    _lenght++;
    if (longitud() == 1){
        _head = nuevoNodo;
    }
}

void Lista::eliminar(Nat i) {
    assert(longitud() > 0 && i >= 0 && i < longitud());
    Nodo *tmp = _head;
    for (int j = 0; j != i; j++) {
        tmp = tmp->_next;
    }
    (tmp->_next != nullptr) ? tmp->_next->_prev = tmp->_prev : _last = tmp->_prev;
    (tmp->_prev != nullptr) ? tmp->_prev->_next = tmp->_next : _head = tmp->_next;
    --_lenght;
    delete tmp;
}

int Lista::longitud() const {
    return _lenght;
}

const int& Lista::iesimo(Nat i) const {
    assert(i >= 0 && i < longitud());
    Nodo* tmp = _head;
    for (int j = 0; j != i; j++) {
        tmp = tmp->_next;
    }
    return tmp->_data;
}

int& Lista::iesimo(Nat i) {
    assert(i >= 0 && i < longitud());
    Nodo* tmp = _head;
    for (int j = 0; j != i; j++) {
        tmp = tmp->_next;
    }
    return tmp->_data;
}

void Lista::mostrar(ostream& o) {
    // Completar
}
