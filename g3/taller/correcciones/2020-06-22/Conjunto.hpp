
template <class T>
Conjunto<T>::Conjunto():_raiz(nullptr), _longitud(0){};

template <class T>
Conjunto<T>::~Conjunto() {
    while (_longitud > 0){
        remover(_raiz->valor);
    }
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    bool res = false;
    Nodo* tmp = _raiz;
    while (tmp != nullptr){
        if (tmp->valor == clave){
            res = true;
        }
        tmp = (tmp->valor < clave) ? tmp->der : tmp->izq;
    }
    return res;
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    if(!pertenece(clave)){
        Nodo* ins = new Nodo(clave);
        Nodo* ant = _raiz;
        Nodo* tmp = ant;

        while (tmp != nullptr){
            ant = tmp;
            tmp = (tmp->valor < clave) ? tmp->der : tmp->izq;
        }

        if (ant != nullptr){
            if(ant->valor < clave){
                ant->der = ins;
            } else {
                ant->izq = ins;
            }
        } else {
            _raiz = ins;
        }
        _longitud++;
    }
}

template <class T>
void Conjunto<T>::remover(const T& elem){
    if (pertenece(elem)){
        Nodo* ant = _raiz;
        Nodo* tmp = ant;

        while (tmp->valor != elem){
            ant = tmp;
            tmp = (tmp->valor < elem) ? tmp->der : tmp->izq;
        }

        if (tmp->izq != nullptr && tmp->der != nullptr){
            T valor = predecesorInmediato(tmp->valor);
            remover(valor);
            if (ant->valor < tmp->valor){
                ant->der->valor = valor;
            } else if (ant->valor > tmp->valor){
                ant->izq->valor = valor;
            } else {
                ant->valor = valor;
            }
        } else if (tmp->izq != nullptr || tmp->der != nullptr){
            if (ant->valor < tmp->valor){
                ant->der = (tmp->der != nullptr) ? tmp->der : tmp->izq;
            } else if (ant->valor > tmp->valor){
                ant->izq = (tmp->der != nullptr) ? tmp->der : tmp->izq;
            } else{
                _raiz = (tmp->der != nullptr) ? tmp->der : tmp->izq;
            }
            delete tmp;
            _longitud--;
        } else{
            if (ant->valor < tmp->valor){
                ant->der = nullptr;
            } else if (ant->valor > tmp->valor){
                ant->izq = nullptr;
            } else{
                _raiz = nullptr;
            }
            delete tmp;
            _longitud--;
        }
    }
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave){
    assert(pertenece(clave));
    Nodo* candidato = _raiz;
    Nodo* tmp = _raiz;
    bool change = false;

    while (tmp != nullptr) {
        if (tmp->valor > clave) {
            candidato = (!change || tmp->valor < candidato->valor) ? tmp : candidato;
            change = true;
            tmp = tmp->izq;
        } else {
            tmp = tmp->der;
        }
    }

    return candidato->valor;
}

template<class T>
const T Conjunto<T>::predecesorInmediato(const T clave){
    assert(pertenece(clave));

    Nodo* tmp = _raiz;
    while (tmp->valor != clave){
        tmp = (tmp->valor < clave) ? tmp->der : tmp->izq;
    }

    Nodo* predecesorInmediato = tmp->izq;
    tmp = predecesorInmediato;
    while (tmp != nullptr){
        predecesorInmediato = tmp;
        tmp = tmp->der;
    }

    return predecesorInmediato->valor;
}

template <class T>
const T& Conjunto<T>::minimo() const {
    assert(_longitud > 0);
    Nodo* ant = _raiz;
    Nodo* tmp = ant;

    while (tmp != nullptr){
        ant = tmp;
        tmp = tmp->izq;
    }

    return ant->valor;
}

template <class T>
const T& Conjunto<T>::maximo() const {
    assert(_longitud > 0);
    Nodo* ant = _raiz;
    Nodo* tmp = ant;

    while (tmp != nullptr){
        ant = tmp;
        tmp = tmp->der;
    }

    return ant->valor;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return _longitud;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}

