template <typename T>
string_map<T>::string_map(): _raiz(new Nodo()), _size(0){}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; }

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    while (!_claves.empty()){
        this->erase(*_claves.begin());
    }

    for (string c : d._claves) {
        insert(make_pair(c, d.at(c)));
    }
}

template<class T>
void string_map<T>::insert(const pair<string, T>& def) {
    Nodo* tmp = _raiz;
    Nodo* ant = _raiz;
    string str = def.first;

    for (int i = 0; i < str.size() - 1; i++) {
        if (tmp->siguientes[int(str[i])] == nullptr){
            tmp->siguientes[int(str[i])] = new Nodo();
            tmp->definidos++;
        }
        ant = tmp;
        tmp = tmp->siguientes[int(str[i])];
        tmp->padre = ant;
    }
    int asK = int(str[str.size() - 1]);

    if (tmp->siguientes[asK] == nullptr){
        T* val = new T(def.second);
        tmp->siguientes[asK] = new Nodo(val);
        tmp->siguientes[asK]->padre = tmp;
        tmp->definidos++;
        _size++;
        _claves.insert(str);
    } else{
        if (tmp->definicion == nullptr){
            _claves.insert(str);
        }
        tmp = tmp->siguientes[asK];
        tmp->padre = ant;
        *tmp->definicion = def.second;
    }
}

template <typename T>
string_map<T>::~string_map(){
    while (!_claves.empty()){
        this->erase(*_claves.begin());
    }
    _raiz->siguientes.clear();
    delete _raiz;
    _raiz = nullptr;
}

template <typename T>
T& string_map<T>::operator[](const string& clave){
    return at(clave);
}

template <typename T>
int string_map<T>::count(const string& clave) const{
    int res = 1;
    Nodo* tmp = _raiz;

    for (int i = 0; i < clave.size() && tmp != nullptr; i++) {
        if (tmp->siguientes[int(clave[i])] == nullptr) {
            res = 0;
        }
        tmp = tmp->siguientes[int(clave[i])];
    }
    if (tmp == nullptr || tmp->definicion == nullptr){
        res = 0;
    }

    return res;
}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
    Nodo* tmp = _raiz;

    for (int i = 0; i < clave.size() && tmp != nullptr; i++) {
        tmp = tmp->siguientes[int(clave[i])];
    }
    return *tmp->definicion;
}

template <typename T>
T& string_map<T>::at(const string& clave) {
    Nodo* tmp = _raiz;

    for (int i = 0; i < clave.size(); i++) {
        tmp = tmp->siguientes[int(clave[i])];
    }
    return *tmp->definicion;
}

template <typename T>
void string_map<T>::erase(const string& clave) {
    Nodo* ant = _raiz;
    Nodo* tmp = ant;

    for (int i = 0; i < clave.size(); i++) {
        tmp = tmp->siguientes[int(clave[i])];
    }

    delete tmp->definicion;
    tmp->definicion = nullptr;
    _size--;
    int strPos = clave.size() - 1;

    while (tmp->definidos == 0 && tmp->definicion == nullptr && tmp != _raiz){
        tmp->siguientes.clear();
        ant = tmp;
        tmp = tmp->padre;
        tmp->siguientes[int(clave[strPos])] = nullptr;
        strPos--;
        delete ant;
        tmp->definidos--;
    }
    _claves.erase(clave);
}

template <typename T>
int string_map<T>::size() const{
    return _size;
}

template <typename T>
bool string_map<T>::empty() const{
    return size() == 0;
}