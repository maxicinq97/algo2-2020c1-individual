#ifndef STRING_MAP_H_
#define STRING_MAP_H_

#include <string>

using namespace std;

template<typename T>
class string_map {
public:

    string_map();

    string_map(const string_map<T>& aCopiar);

    string_map& operator=(const string_map& d);

    ~string_map();

    void insert(const pair<string, T>&);

    int count(const string &key) const;

    const T& at(const string& key) const;
    T& at(const string& key);

    void erase(const string& key);

    int size() const;

    bool empty() const;

    T &operator[](const string &key);

private:

    struct Nodo {
        Nodo(): definicion(nullptr), siguientes(256, nullptr), definidos(0), padre(nullptr){};
        Nodo(T* def): definicion(def), siguientes(256, nullptr), definidos(0), padre(nullptr){};
        Nodo* padre;
        vector<Nodo*> siguientes;
        int definidos;
        T* definicion;
    };

    set<string> _claves;
    Nodo* _raiz;
    int _size;
};

#include "string_map.hpp"

#endif // STRING_MAP_H_
