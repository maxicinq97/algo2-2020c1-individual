#include <iostream>

using namespace std;

using uint = unsigned int;
const float PI = 3.14;

// Ejercicio 1

class Rectangulo {
    public:
        Rectangulo(uint alto, uint ancho);
        uint alto();
        uint ancho();
        float area();

    private:
        int alto_;
        int ancho_;

};

Rectangulo::Rectangulo(uint alto, uint ancho) : alto_(alto), ancho_(ancho) {};

uint Rectangulo::alto() {
    return this->alto_;
}

uint Rectangulo::ancho() {
    return this->ancho_;
}

float Rectangulo::area() {
    return ancho()*alto();
}

// Ejercicio 2

class Elipse{
    public:
        Elipse(uint a, uint b) : r_a_(a), r_b_(b){};
        uint r_a(){
            return this->r_a_;
        }
        uint r_b(){
            return this->r_b_;
        }
        float area(){
            return PI * r_b() * r_a();
        }

    private:
        int r_a_;
        int r_b_;
};

// Ejercicio 3

class Cuadrado {
    public:
        Cuadrado(uint lado);
        uint lado();
        float area();

    private:
        Rectangulo r_;
};

Cuadrado::Cuadrado(uint lado): r_(lado, lado) {}

uint Cuadrado::lado() {
    return this->r_.alto();
}

float Cuadrado::area() {
    return this->lado()*lado();
}

// Ejercicio 4

class Circulo{
    public:
        Circulo(uint radio): r_(radio, radio) {};
        uint radio(){
            return this->r_.r_a();
        };
        float area(){
            return this->r_.area();
        };

    private:
        Elipse r_;
};


// Ejercicio 5

ostream& operator<<(ostream& os, Rectangulo r) {
    os << "Rect(" << r.alto() << ", " << r.ancho() << ")";
    return os;
}

// ostream Elipse

ostream& operator<<(ostream& os, Elipse e) {
    os << "Elipse(" << e.r_a() << ", " << e.r_b() << ")";
}

// Ejercicio 6

ostream& operator<<(ostream& os, Circulo c){
    os << "Circ(" << c.radio() << ")";
}

ostream& operator<<(ostream& os, Cuadrado c){
    os << "Cuad(" << c.lado() << ")";
}

