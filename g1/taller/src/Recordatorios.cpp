#include <iostream>
#include <list>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    Fecha(int mes, int dia): mes_(mes) , dia_(dia){}
    int mes(){
        return this->mes_;
    }
    int dia() {
        return this->dia_;
    }
    bool operator==(Fecha f){
        return (this->mes_ == f.mes() &&
                    this->dia_ == f.dia());
    }
    void incrementar_dia(){
        if (this->dia() < dias_en_mes(this->mes())){
            this->dia_++;
        } else{
            this->dia_ = 1;
            this->mes_ = ++this->mes_ % 12;
        }
    }

  private:
    int mes_;
    int dia_;
};

ostream& operator<<(ostream& os, Fecha f){
    os << f.dia() << "/" << f.mes();
    return os;
}

// Ejercicio 11, 12

class Horario{
    public:
    Horario(uint hora, uint min): hora_(hora), min_(min){};
    uint hora(){
        return this->hora_;
    }
    uint min(){
        return this->min_;
    }
    bool operator==(Horario h){
        return this->hora() == h.hora() &&
                this->min() == h.min();
    }
    bool operator<(Horario h){
        bool res = false;
        if(this->hora() == h.hora()){
            res = this->min() < h.min();
        } else {
            res = this->hora() < h.hora();
        }
        return res;
    }
    private:
    uint hora_;
    uint min_;
};

ostream& operator<<(ostream& os, Horario h){
    os << h.hora() << ":" << h.min();
    return os;
}


// Ejercicio 13

class Recordatorio{
    public:
        Recordatorio(Fecha f, Horario h, string m): fecha_(f), horario_(h), mensaje_(m){};
        string mensaje(){
            return this->mensaje_;
        }
        Fecha fecha(){
            return this->fecha_;
        }
        Horario horario(){
            return this->horario_;
        }
        bool operator<(Recordatorio r){
            bool res = this->horario() < r.horario();
            return res;
        }
    private:
        Fecha fecha_;
        Horario horario_;
        string mensaje_;
};

ostream& operator<<(ostream& os, Recordatorio r){
    os << r.mensaje() << " @ " << r.fecha() << " " << r.horario();
    return os;
}

// Ejercicio 14

class Agenda{
    public:
        Agenda(Fecha f): hoy_(f) {};
        void agregar_recordatorio(Recordatorio rec){
            recordatorios_.push_back(rec);
        }
        void incrementar_dia(){
            hoy_.incrementar_dia();
        }
        list<Recordatorio> recordatorios_de_hoy(){
            list<Recordatorio> res;
            for (Recordatorio r : recordatorios_) {
                if (r.fecha() == hoy()) {
                    res.push_back(r);
                }
            }
            res.sort();
            return res;
        }
        Fecha hoy(){
            return hoy_;
        }
    private:
        list<Recordatorio> recordatorios_;
        Fecha hoy_;
};

ostream& operator<<(ostream& os, Agenda a){
    os << a.hoy() << endl << "=====" << endl;
    for (Recordatorio r : a.recordatorios_de_hoy()) {
        os << r << endl;
    }
    return os;
}

